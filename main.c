/* CSE 202 Bits Makeup
 * Student Name: yiqun xu
 * Student Email:yix223@lehigh.edu
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include<math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
/* we use this union in main to pass a float as an unsigned */
union Converter {
    unsigned asInt;
    float asFloat;
};

void usage(void) {
  printf("Usage: shell [-hma]\n");
  printf("-h print this dialogue\n");
  printf("-m [val1] [val2] add val1 and val2\n");
  printf("-a [val1] [val2] add val1 and val2\n");
  exit(1);
}

/* mul_floats(unsigned f1, unsigned f2) should return the bit-level
 * representation of [f1 * f2] for floating-point values f1 and f2.
 * You may not use any floating-point operations to solve this problem.
 */

unsigned mul_floats(unsigned f1, unsigned f2)
{
  uint64_t round_frac;
  uint64_t round_frac1;
 unsigned sign_f1 = (f1 & 0x80000000) >> 31;
 unsigned exp_f1 = ((f1 & 0x7f800000)  >> 23);
 unsigned frac_f1 = (f1 & 0x007FFFFF); 
 unsigned sign_f2 = (f2 & 0x80000000) >> 31;
 unsigned exp_f2 = ((f2 & 0x7f800000) >> 23 );
 unsigned frac_f2 = (f2 & 0x007FFFFF); 

     /* Checks for NaN */
  if ((f1 & ~0x80000000u) > 0x7f800000) {
    printf("\n/1 NAN/\n");
 return f1 | 0x00400000;
 }
 if ((f2 & ~0x80000000u) > 0x7f800000) {
   printf("\n/2 NAN/\n");
 return f2 | 0x00400000;
 }
       if(exp_f1>=0xFF&&f2==0){//exp overflow
      return (1 << 31) | ( (255)  << 23) | 1;//negative NAN
    }
      if(exp_f2>=0xFF&&f1==0){//exp overflow
      return (1 << 31) | ( (255)  << 23) | 1;//negative NAN
    }
 if(exp_f1==0&&exp_f2==0){//treat denomalize number as 0
   if(sign_f1==sign_f2)
   return (0 << 31) | ( 0  << 23) | 0;//postive 0;
   else{
   return (1 << 31) | ( 0  << 23) | 0;}//negative 0
 }
  if(exp_f1>=0xFF||exp_f2>=0xff){//exp overflow
      if(sign_f1==sign_f2){
      return (0 << 31) | ( (255)  << 23) | 0;//postive inf.
      }
      return (1 << 31) | ( (255)  << 23) | 0;//negative inf
    }
  uint64_t new_frac =0;//old frac and new frac are used to store the frac after multiplication
  uint64_t old_frac =0;
  uint64_t temp;
  int index=0;
  frac_f1 =  (1  << 23) | (frac_f1);//add 1 in the beginning
  frac_f2 =  (1  << 23) | (frac_f2);//add 1 in the begining
  
  for(index=0;index<24;index++){//multiply mantasa
  old_frac=frac_f1<<(31-index);
  old_frac=old_frac>>31;
  temp = (old_frac * frac_f2)<<index;
  new_frac+=temp;
  }//*/
  //save new_frac into round frac for future use;
  round_frac=new_frac;
  unsigned new_exp;

  //printBits(sizeof(new_frac), &new_frac);
  if(new_frac>(140737488355327)){//significant overflow. 0b11111111111111111111111111111111111111111111111
  //save new_frac into round frac for future use;
  round_frac=round_frac<<40;
  round_frac=round_frac>>40;//get the 24 digit we clean
  new_frac=new_frac>>24;//clean the extra 1 bit becoz of overlow
  new_frac=new_frac<<41;//clean the extra bits
  new_frac=new_frac>>41;//clean the extra bits
  new_exp =  (exp_f2-127) + (exp_f1-127)+127+1;//add one bit becoz of overflow
   // printBits(sizeof(round_frac), &round_frac);
  if(round_frac>0b100000000000000000000000){//rounding
     new_frac=new_frac+1;//round up
  }else if (round_frac==0b100000000000000000000000){
    //round to even
  }//else round down.
  //*/
  }else{//significant didnt overflow.
  //save new_frac into round frac for future use;
  round_frac=round_frac<<41;
  round_frac=round_frac>>41;//get the 23 digit we clean
  new_frac=new_frac>>23;//clean the extra bits
  new_frac=new_frac<<41;//clean the extra bits
  new_frac=new_frac>>41;//clean the extra bits
  new_exp =  (exp_f2-127) + (exp_f1-127)+127;//get the exp.
   // printBits(sizeof(round_frac), &round_frac);
  if(round_frac>0b10000000000000000000000){//rounding
     new_frac=new_frac+1;//round up
  }else if (round_frac==0b100000000000000000000000){
    //round to even
  }//else round down.
  //*/
  }
  unsigned new_sign;
  
  if(sign_f1==sign_f2){//identify the sign if both sign equal then the sign is posstive else negatve
   new_sign=0;
  }else{
   new_sign=1;
  }
  //////////////////what i need to do is round 
  ////use the grs table to check the last 3 digits.
  unsigned result= (new_sign << 31) | new_exp << 23 | new_frac;
 return result;//result for mul
}

/* add_floats(unsigned f1, unsigned f2) should return the bit-level
 * representation of [f1 + f2] for floating-point values f1 and f2.
 * You may not use any floating-point operations to solve this problem.
 */

unsigned add_floats(unsigned f1, unsigned f2) {
unsigned roun_frac2afteradd;
 if(f1==0){//cmp f1/f2 with0
   return f2;//f2+0=f2
 }else if(f2==0){
   return f1;//f1+0=f1
 }else if(f2==0&&(f1==0)){
   return 0;//0+0=0
 }
  unsigned NAN_=2143289344;//get the NAN
 unsigned INF_=4286578688;//get the _inf
 unsigned sign_INF = (INF_ & 0x80000000) >> 31;//get the _inf
 sign_INF = sign_INF ^sign_INF;//get the _inf
 unsigned sign_f1 = (f1 & 0x80000000) >> 31;
 unsigned exp_f1 = ((f1 & 0x7f800000)  >> 23);
 unsigned frac_f1 = (f1 & 0x007FFFFF); 
 unsigned sign_f2 = (f2 & 0x80000000) >> 31;
 unsigned exp_f2 = ((f2 & 0x7f800000) >> 23 );
 unsigned frac_f2 = (f2 & 0x007FFFFF);
 unsigned sign = sign_f1 ^ sign_f2;
 if(exp_f2==0&&(exp_f1==0)){//treat denormalize as 0
   return 0;
 }if(exp_f1==0){//treat denormalize num as 0
   return f2;
 }else if(exp_f2==0){//treat denormalize as 0
   return f1;
 }
    /* Checks for NaN */
  if ((f1 & ~0x80000000u) > 0x7f800000) {
    //printf("\n/1 NAN/\n");
 return f1 | 0x00400000;
 }
 if ((f2 & ~0x80000000u) > 0x7f800000) {
   //rintf("\n/2 NAN/\n");
 return f2 | 0x00400000;
 }
 /*check for inf*/
 if(exp_f1>=0xFF&&exp_f2>=0XFF){
   if(sign_f1==0&&sign_f2==1)//
  return (1 << 31) | ( (255)  << 23) | 1;//-NAN
     if(sign_f1==1&&sign_f2==0)
  return (1 << 31) | ( (255)  << 23) | 1;//-NAN
 }
  if(exp_f1>=0xFF){//exp overflow
      if(sign_f1==0){
      return sign_INF | 0x7f800000;//postive inf
      }
      return (1 << 31) | ( (255)  << 23) | 0;//negative inf
    }
          if(exp_f2>=0xFF){//exp overflow
      if(sign_f2==0){
      return sign_INF | 0x7f800000;//postive inf
      }
      return (1 << 31) | ( (255)  << 23) | 0;//- inf
    }

unsigned round_frac;
///normal add.
unsigned temp_sign;
unsigned temp_exp;
unsigned temp_frac;
int index;
if( ((sign_f1+sign_f2)==0) || ((sign_f1+sign_f2)==2) ){//addition 
if(exp_f1==exp_f2){//when exp are equal no shift.
  temp_exp=exp_f1+1;//increment exp;
      if(temp_exp>=0xFF){//exp overflow
      if(sign_f1==0){
      return sign_INF | 0x7f800000;//return INF
      }
      return (1 << 31) | ( (255)  << 23) | 0;//return -INF
    }
  temp_frac=frac_f1+frac_f2;//add frac togther
  round_frac=temp_frac;
  round_frac=round_frac<<31;
  round_frac=round_frac>>31;
  temp_frac=temp_frac>>1;//shift one bit;
  if(round_frac==1){
    if(( ((temp_frac)<<31) >>31 ) ==0){
      }//rount to even;
      else{
    temp_frac+=1;//shift one bit;
    }
  }//*/
}else if(exp_f1!=exp_f2){//when exp are not equal 
  unsigned shiftnumber;
  unsigned shiftfrac;
  //check how is bigger.
  if(exp_f1>exp_f2){//f1 is bigger;
  shiftnumber=exp_f1-exp_f2;//this is how many position we need to shift
  round_frac=((1  << 23) |frac_f2);//frac before shifting
  round_frac=round_frac<<(32-shiftnumber);//get the frac fragment we clean.
  round_frac=round_frac>>(32-shiftnumber);//get the frac fragment we clean.
  shiftfrac=((1  << 23) |frac_f2)>>shiftnumber;//this is frac after shift
  temp_frac=shiftfrac+frac_f1;//add the bigger frac with the one after shift.
    if(shiftnumber==1){
    if(round_frac==1){
      if(( ((shiftfrac+frac_f1+1)<<31) >>31 ) ==1){
//round to even
      }else{
    temp_frac=shiftfrac+frac_f1+1;}//round to even
    }
    }else if(round_frac==pow (2, shiftnumber-1)){
      if(( ((shiftfrac+frac_f1)<<31) >>31 ) ==0){
        temp_frac=shiftfrac+frac_f1;
      }//rount to even;
      else{
    temp_frac=shiftfrac+frac_f1+1;
    }
  }else if(round_frac>pow (2, shiftnumber-1)){
    temp_frac=shiftfrac+frac_f1+1;
    }
  
  temp_exp=exp_f1;//save the bigger exp in temp which is f1;
  }else{//f2 is bigger;
  shiftnumber=exp_f2-exp_f1;// this is how many position we need to shift.
  
  round_frac=((1  << 23) |frac_f1);//frac before shifting
  round_frac=round_frac<<(32-shiftnumber);//get the frac fragment we clean.
  round_frac=round_frac>>(32-shiftnumber);//get the frac fragment we clean.
  shiftfrac=((1  << 23) |frac_f1)>>shiftnumber;//this is frac after shift
  temp_frac=shiftfrac+frac_f2;//add the bigger frac with the one after shift.
    if(shiftnumber==1){
    if(round_frac==1){
      if(( ((shiftfrac+frac_f2+1)<<31) >>31 ) ==1){
//round to even
      }else{
    temp_frac=shiftfrac+frac_f2+1;}//round to even
    }
    }else if(round_frac==pow (2, shiftnumber-1)){
      if(( ((shiftfrac+frac_f2)<<31) >>31 ) ==0){
        temp_frac=shiftfrac+frac_f2;
      }//rount to even;
      else{
    temp_frac=shiftfrac+frac_f2+1;
    }
  }else if(round_frac>pow (2, shiftnumber-1)){
    temp_frac=shiftfrac+frac_f2+1;
    }

  temp_exp=exp_f2;//save the bigger exp in temp which is f2;
  }// end of (exp_f1>exp_f2);//check how is bigger.
}//end of (exp_f1!=exp_f2) //when exp are not equal
   if(temp_frac>0x7FFFFF){//if frac overflow
     temp_exp=temp_exp+1;//exp+1;
      if(temp_exp>=0xFF){//exp overflow
      if(sign_f1==0){
      return sign_INF | 0x7f800000;//return INF
      }
      return (1 << 31) | ( (255)  << 23) | 0;//return -INF
    }
     round_frac=temp_frac<<31;
     round_frac=round_frac>>31;
     temp_frac=temp_frac<<9;
     temp_frac=temp_frac>>9;//delete the hidden value.
     temp_frac=temp_frac>>1;//shift right for 1 unit becoz of the overflow.
      if(round_frac ==1){
          if(( ((temp_frac+1)<<31) >>31 ) ==1){
//round to even
      }else{
    temp_frac=temp_frac+1;}//round to even
      }else{//work for  // 38.646030+25.694679 
   // temp_frac+=1;
    }//*/
   }  //end of detect overflow;
    temp_sign=sign_f1;//temp_sign = sign_f1/sign_f2 doesnt matter they are equal.
}//end of addition


else if( ((sign_f1+sign_f2)==1) ){//subtraction
  unsigned num;//the flaoting point that i will return
  unsigned abs_f1=(0 << 31) | (exp_f1  << 23) | (frac_f1);//absolute value
  unsigned abs_f2=(0 << 31) | (exp_f2  << 23) | (frac_f2);//absolute value
  if(abs_f1>abs_f2){//cmp absolute value of f1 and f2
  unsigned new_frac=frac_f2;
  if(exp_f1!=exp_f2){
  new_frac =  (1  << 23) | (frac_f2);//hiden num=1 when its normalize  //Add implict 1 
  }
  if(exp_f1!=exp_f2){
  new_frac=new_frac>>(exp_f1-exp_f2);}
    unsigned sum;
    sum = frac_f1-(new_frac);
    if(sum>0x7FFFFF){//significant overflow
    sum=sum<<9;
    sum=sum>>9;
    sum=sum<<1;
    //sum-=1;
    exp_f1=exp_f1-1;//decrement exp.
      if(exp_f2<=0){//exp underflow
      printf("exp underflow");
      if(sign_f1==0){
      return NAN_ | 0x00400000;//return NAN
      }
      return NAN_ | 0x00400000;//return NAN
    }
    }//*/
    if(exp_f1==exp_f2){//when equal
  int index=0;
  check_if:   if(sum<=0x7FFFFF){//shift left position until the frac recover its hidden bit.
  index++;//count the shift position
    sum=sum<<1;//shift until the frac recover its hidden bit
    goto check_if;
}
    exp_f1 = exp_f1-index;
      if(exp_f1<=0){//exp underflow
      printf("exp underflow");
      if(sign_f1==0){
      return NAN_ | 0x00400000;//return NAN
      }
      return NAN_ | 0x00400000;//return NAN
    }
   sum= sum<< 9;//delete the extra hidden 1
   sum= sum>> 9;
   //sum-=index*2;
    }
    
  num = (sign_f1 << 31) | (exp_f1  << 23) | sum;//use the bigger exp. 
  }else if (abs_f1<abs_f2){//f2 is bigger.
  unsigned new_frac;
  new_frac=frac_f1;
  if(exp_f1!=exp_f2){
  new_frac =  (1  << 23) | (frac_f1);//hiden num=1 when its normalize  //Add implict 1
  }
  if(exp_f1!=exp_f2){
  new_frac=new_frac>>(exp_f2-exp_f1);
  }
  unsigned shiftnum = exp_f2-exp_f1;
  unsigned sum;
    sum = frac_f2-(new_frac); 
    if(sum>0x7FFFFF){//significant overflow
    sum=sum<<9;//delete the extra hidden num
    sum=sum>>9;
    sum=sum<<1;//shift one becoz of the overflow.
    exp_f2=exp_f2-1;//decrement exp becoz of the overflow;
          if(exp_f1<=0){//exp underflow
     // printf("exp underflow");
      if(sign_f2==0){
      return NAN_ | 0x00400000;//return NAN
      }
      return NAN_ | 0x00400000;//return NAN
    }
    }
    if(exp_f1==exp_f2){//when exps are equal
  int index=0;
  check_if2:  if(sum<=0x7FFFFF){//shift left position until the frac recover its hidden bit.
    index++;//count the shift position
    sum=sum<<1;//shift until the frac recover its hidden bit
  goto check_if2;
    }
    exp_f2 = exp_f2-index;
      if(exp_f2<=0){//exp underflow
    //  printf("exp underflow");
      if(sign_f2==0){
      return NAN_ | 0x00400000;//return NAN
      }
      return NAN_ | 0x00400000;//return NAN
    }
   sum= sum<< 9;//delete the extra hidden num
   sum= sum>> 9;
    }

  num = (sign_f2 << 31) | ( exp_f2  << 23) | sum;//use the bigger exp. 
  }else{//divide itself =0;
    return 0;
  }
 return num;
}//end of subtraction
 return (temp_sign << 31) | (temp_exp  << 23) | (temp_frac);//for addition
}

int main(int argc, char **argv) {
  char c;
  unsigned result;
  union Converter val1;
  union Converter val2;
 
  c = getopt(argc, argv, "hi:ma"); /* parse arguments */
  switch(c) {
    case 'h':      /* print help message */
      usage();
      break;
    case 'm':      /* multiply */
      val1.asFloat = atof(argv[2]);
      val2.asFloat = atof(argv[3]);
      result = mul_floats(val1.asInt, val2.asInt);
      break;
    case 'a':      /* add */
      val1.asFloat = atof(argv[2]);
      val2.asFloat = atof(argv[3]);
      result = add_floats(val1.asInt, val2.asInt);
      break;
    default:
      usage();
      break;
  }
  printf("%08x\n", result);
}
